use std::{collections, process, io::{self, BufRead, Write}};
use anyhow::{anyhow, Context};

fn print_obscured_word(word: &str, guessed_letters: &collections::BTreeSet<char>){
    println!();
    for letter in word.chars(){
        if guessed_letters.contains(&letter) {
            print!(" {}", letter);
        } else {
            print!(" _");
        }
    }
    println!();
    println!();
}

fn main() -> anyhow::Result<()> {
    let word_command_output = process::Command::new("sh")
        .arg("-c")
        .arg("cat /usr/share/dict/words | grep -P '^[\\x61-\\x7a]+$' | shuf -n1")
        .output()
        .context("Failed to get word")?;

    if !word_command_output.status.success() {
        return Err(anyhow!("Word command failed."));
    }

    let word = std::str::from_utf8(&word_command_output.stdout)
        .context("Output contained invalid UTF-8")?
        .trim()
        .to_owned();

    let mut stdout = io::stdout();
    let stdin = io::stdin();
    let mut locked_stdin = stdin.lock();
    let mut input_buffer = String::new();


    let mut guesses_left = 10;
    let mut guessed_letters = collections::BTreeSet::<char>::new();

    print_obscured_word(&word, &guessed_letters);

    loop {
        println!();
        print!("Guess a letter: ");
        stdout.flush().context("Error while flushing stdout")?;

        input_buffer.clear();
        let read_line_result = locked_stdin.read_line(&mut input_buffer);

        if let Ok(0) = read_line_result {
            println!("Exiting.");
            return Ok(());
        }

        // trim the input
        input_buffer.truncate(input_buffer.trim_end().len());

        // Get exactly one character from the input
        let mut input_buffer_chars = input_buffer.chars();
        let guess_option = input_buffer_chars.next();
        let guess;
        if let Some(guess_char) = guess_option {
            guess = guess_char;
        } else {
            println!("Please enter a letter.");
            continue;
        }
        if input_buffer_chars.next().is_some() {
            println!("One letter at a time please.");
            continue;
        }

        // guess now contains the entered character

        if !('a'..='z').contains(&guess) {
            println!("The words only contain lowercase a through z.");
            continue;
        }

        if guessed_letters.contains(&guess) {
            println!("You've already used that letter.");
            continue;
        }

        guessed_letters.insert(guess);

        print_obscured_word(&word, &guessed_letters);

        if word.chars().all(|l| guessed_letters.contains(&l)) {
            println!();
            println!("🎉 You Win! 🎉");
            println!("👏👏👏👏👏👏👏");
            return Ok(());
        }

        println!();

        // Guess is incorrect if none of the letters in the word are the same.
        if !word.chars().any(|l| l == guess) {
            guesses_left -= 1;
            println!("You have {} wrong guesses left.", guesses_left);
        }

        if guesses_left <= 0 {
            break;
        }

        print!("Letters you've used: ");
        for letter in guessed_letters.iter() {
            print!("{} ", letter);
        }
        println!();
    }

    println!("😢 You lost, the word was \"{}\"", &word);

    Ok(())
}
